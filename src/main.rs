mod blog;

#[macro_use] extern crate rocket;

use rocket::{Request, fs::FileServer};
use rocket_dyn_templates::{Template, context};
use chrono::{Local, TimeZone};

#[get("/")]
fn index() -> Template {
    Template::render("pages/index", context!{
        time: Local::now()
            .format("%H:%M")
            .to_string()
    })
}

#[get("/about")]
fn about() -> Template {
    let age = Local.timestamp(1096840800, 0).elapsed_years();
    Template::render("pages/about", context!{ age })
}

#[get("/contact")]
fn contact() -> Template {
    Template::render("pages/contact", context!{})
}

#[get("/setup")]
fn setup() -> Template {
    Template::render("pages/setup", context!{})
}

#[get("/projects")]
fn projects() -> Template {
    Template::render("pages/projects", context!{})
}

#[get("/profiles")]
fn profiles() -> Template {
    Template::render("pages/profiles", context!{})
}

#[get("/interesting")]
fn interesting() -> Template {
    Template::render("pages/interesting", context!{})
}

#[catch(404)]
fn error_404(_req: &Request) -> Template {
    Template::render("errors/404", context!{})
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .register("/", catchers![error_404])
        .mount("/", routes![index, blog::blog, blog::atom_feed, about, contact, setup, profiles, projects, interesting])
        .mount("/", FileServer::from("files"))
        .attach(Template::fairing())
}
